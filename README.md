# Docker images for (Legacy) RPC Online Software

## How to build a new image

Push a new commit, then, on the Side Bar, got he CI/CD --> Pipelines. Select the commit you want to build, by clicking on the Pipeline number. Click on `buildimage` to run the Pipeline.

## How to launch a container

Login to CERN Gitlab Registry (only once).

```
docker login gitlab-registry.cern.ch
```

Pull the image.

```
docker pull gitlab-registry.cern.ch/ftorresd/ci-sandbox/cmsos15_ts_ts-only
```

From your development directory, launch the container.

```
docker run --user $(id -u `whoami`):$(id -g `whoami`) \
--rm -it \
-v `pwd`:/home/cmsusr/rpct_xdaq15 \
-p 1972:1972 \
gitlab-registry.cern.ch/cmsrpconline/docker/cmsos15_ts_master
```

On MacOS, ```--user $(id -u `whoami`):$(id -g `whoami`)``` is not needed.

Note: This image is for development, only.

